﻿using UnityEngine;
using UnityEditor;
using UnityEngine.TestTools;
using NUnit.Framework;

namespace Shorka
{
    public class TestMathfIntPow
    {

        [Test]
        public void TestMathfIntPow_RaiseTo0()
        {
            int rnd = Random.Range(-20, 20);

            Assert.AreEqual(Mathf.Pow(rnd, 4), MathfFaster.PowInt(rnd, 0));
        }

        [Test]
        public void TestMathfIntPow_Raise2To4()
        {
            Assert.AreEqual(Mathf.Pow(2, 4), MathfFaster.PowInt(2, 4));
        }

        [Test]
        public void TestMathfIntPow_RaiseRndPositive()
        {
            int rnd = Random.Range(1, 20);
            Assert.AreEqual(Mathf.Pow(4, rnd),
                MathfFaster.PowInt(4, rnd));
        }

        [Test]
        public void TestMathfIntPow_Raise13To1()
        {
            Assert.AreEqual(Mathf.Pow(13, 1), MathfFaster.PowInt(13, 1));
        }

        [Test]
        public void TestMathfIntPow_RaiseRndNegative()
        {
            int rnd = Random.Range(-10, 0);
            Assert.AreEqual(Mathf.Pow(4, rnd),
                MathfFaster.PowInt(4, rnd));
        }
    }
}