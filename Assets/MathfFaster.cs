﻿namespace Shorka
{
    public static class MathfFaster
    {

        /// <summary>
        /// Returns float number raised to int power. 
        /// </summary>
        public static float PowInt(float numb, int power)
        {
            numb = power > 0 ? numb : 1 / numb;
            int n = power > 0 ? power : power * (-1);

            float res = 1;
            for (int i = 0; i < n; i++)
                res *= numb;         

            return res;
        }
    }
}